﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tweetinvi;
using capa_modelo;
using System.Collections;
using System.Drawing;
using Tweetinvi.Parameters;
using Tweetinvi.Core.Parameters;

namespace capa_datos
{
    public class Twitter
    {
        /*Se supone que los cojera de la clase userApp*/
        private string CONSUMER_KEY;//= "FIagYsQNtkbYWmvHcxTB2owUy";
        private string CONSUMER_SECRET;// = "dPRlKf20pTG1TkhNDuL1w6MlGB1yBH97atzAA0TitTK4AOnHBa";
        private string ACCESS_TOKEN;// = "1291967058-iMNUmrD76zjQFA771ydsm2dtkYM77yascyTUAQB";
        private string ACCESS_TOKEN_SECRET;// = "OXQQkw2jtDlCqGqKcbL6v5xWFaYzMO0qwTpRrTG3rmppq";
        UserApp miuser;
        private Tweetinvi.Models.IAuthenticatedUser user;
        DatosUsuario datos_user;

        public Twitter(UserApp temp)
        {
            miuser = temp;
            CONSUMER_KEY = temp.ConsumerKey;
            CONSUMER_SECRET = temp.ConsumerKeyApi;
            ACCESS_TOKEN = temp.AccessToken;
            ACCESS_TOKEN_SECRET = temp.AccessTokenSecret;
            ObtenerDatosUsuario();

        }

        private void ObtenerDatosUsuario()
        {
            Auth.SetUserCredentials(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN,
            ACCESS_TOKEN_SECRET);
            user = User.GetAuthenticatedUser();
            string imagen = user.ProfileImageUrl400x400;
            DateTime fecha = user.CreatedAt;
            string descripcion = user.Description;
            int friendsCount = user.FriendsCount;
            int follersCount = user.FollowersCount;
            String lengua = user.Language.ToString();
            String location = user.Location;
           
            
            datos_user = new DatosUsuario(imagen, fecha, friendsCount, follersCount,
                descripcion, lengua, location);
            datos_user.nombre = user.Name;
        }


        /*Obtener timeLine*/
        public List<MiTweet> ObtenerTimeLine()
        {
            List<MiTweet> tweets = new List<MiTweet>();
            Auth.SetUserCredentials(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, 
                ACCESS_TOKEN_SECRET);
            user = User.GetAuthenticatedUser();
           
            // Get the latest 10 tweets published on your timeline
            var timelineTweets = Timeline.GetHomeTimeline(10);
            foreach (var timelineTweet in timelineTweets)
            {
              //  tweets.Add( timelineTweet.ToString() + Environment.NewLine);
                long id=timelineTweet.Id;
                string text = timelineTweet.Text;
                string time = timelineTweet.CreatedAt.ToShortDateString();
                int retweets = timelineTweet.RetweetCount;
                int favourite = timelineTweet.FavoriteCount;
                //PROBLEMAS CON EL CUOTE
                int quote = 0; //(int)timelineTweet.QuotedStatusId;
                
                Tweetinvi.Models.IUser user= timelineTweet.CreatedBy;
                string user_id = user.Name;
                string imagen = user.ProfileImageUrl;
           

                MiTweet tmp = new MiTweet(id, text, time, retweets,
                    favourite, quote, user_id,imagen);

                if (timelineTweet.Media.Count != 0)
                {
                    string imagen_tweet = timelineTweet.Media[0].MediaURLHttps;
                    tmp.imagen_tweet = imagen_tweet;
                }
                else
                    tmp.imagen_tweet = null;
                tweets.Add(tmp);

            }
            return tweets;
        }

        public List<FollowersDB> ObtenerFollowersDB()
        {
            List<FollowersDB> follows = new List<FollowersDB>();
            Auth.SetUserCredentials(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN,
            ACCESS_TOKEN_SECRET);
            var user = User.GetAuthenticatedUser();
            var followers = User.GetFollowers(Convert.ToInt64(miuser.Twitter_ID), 10);
            /*Recibir la lista con los usuarios de forma correcta*/
            /*Tengo que crear una clase con followers*/
            foreach (var follower in followers)
            {
                string line = follower.Name;
                FollowersDB temp = new FollowersDB();
                temp.id_follower = follower.Id;
                temp.Nombre = follower.Name;
                temp.Imagen = follower.ProfileImageUrl400x400;
                temp.Fecha_Creacion = follower.CreatedAt;
                temp.ContSeguidores = follower.FollowersCount;
                temp.ContFav = follower.FavouritesCount;
                temp.estado = follower.Description;
                temp.IDUsuario = miuser.IDUsuario;
         
                follows.Add(temp);
            }
            return follows;
        }






        /*Con esto obtengo */
        public List<Followers> ObtenerFollowers(UserApp usuario)
        {
            List<Followers> follows = new List<Followers>();
            Auth.SetUserCredentials(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN,
            ACCESS_TOKEN_SECRET);
            var user = User.GetAuthenticatedUser();
            var followers = User.GetFollowers(Convert.ToInt64(miuser.Twitter_ID), 10);
            
            /*Recibir la lista con los usuarios de forma correcta*/
            /*Tengo que crear una clase con followers*/
            foreach (var follower in followers)
            {
                string line = follower.Name;
                Followers temp = new Followers();
                temp.id_follower = follower.Id;
                temp.Nombre = follower.Name;
                temp.Imagen = follower.ProfileImageUrl400x400;
                temp.Fecha_Creacion = follower.CreatedAt;
                temp.ContSeguidores = follower.FollowersCount;
                temp.ContFav= follower.FavouritesCount;
                temp.estado = follower.Description;
                temp.IDUsuario = usuario.IDUsuario;
                temp.following = follower.Following;
               
                follows.Add(temp);
            }
            return follows;
        }

        /*Hacer tweet nuevo*/
        public void HacerNuevoTweet(String texto)
        {
            Auth.SetUserCredentials(CONSUMER_KEY, CONSUMER_SECRET,
                ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
            var user = User.GetAuthenticatedUser();
            var tweet = Tweet.PublishTweet(texto);
        }


        public DatosUsuario DarDatosUsuario()
        {
            return datos_user;
        }

        public void HacerNuevoTweetConfoto(string texto, string imagen)
        {
            Auth.SetUserCredentials(CONSUMER_KEY, CONSUMER_SECRET,
       ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
            var user = User.GetAuthenticatedUser();

            if (imagen != "")
            {
                ImageConverter _imageConverter = new ImageConverter();
                byte[] arrayfoto = (byte[])_imageConverter.ConvertTo(
                    Image.FromFile(imagen), typeof(byte[]));
                var tweet = Tweet.PublishTweetWithImage(texto, arrayfoto);
            }
            else //Cuando modificamos un tweet que tenia foto y se lo eliminamos
            {    //se mete por aqui
                HacerNuevoTweet( texto);
            }
        }

        public void HacerNuevoTweetConfoto2(string titulo, byte[] arrayBytes)
        {
            var tweet = Tweet.PublishTweetWithImage(titulo, arrayBytes);
        }


        public List<Menciones> ObtenerMencionesPorUsuario()
        {
            Auth.SetUserCredentials(CONSUMER_KEY, CONSUMER_SECRET,
            ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
            var user = User.GetAuthenticatedUser();
            var mentionsTimelineParameters = new MentionsTimelineParameters();
            mentionsTimelineParameters.MaximumNumberOfTweetsToRetrieve = 50;
            var tweets = Timeline.GetMentionsTimeline(mentionsTimelineParameters);

            List<Menciones> misMenciones = new List<Menciones>();
            foreach (var mention in tweets)
            {
                Menciones temp = new Menciones();
                temp.IDUsuario = miuser.IDUsuario;
                temp.nombre = mention.CreatedBy.Name;
                temp.foto_usuario = mention.CreatedBy.ProfileImageUrl400x400;
                temp.fecha = mention.CreatedAt;
                temp.color_fondo = mention.CreatedBy.ProfileLinkColor;
                temp.text = mention.Text;
                temp.TweetID = mention.Id;
                temp.favorito = mention.Favorited;
                temp.retweet = mention.Retweeted;
                
                misMenciones.Add(temp);

            }
            return misMenciones;

        }


        public void ObtenerMencionesPorPalabraClave(String palabra)
        {
            Auth.SetUserCredentials(CONSUMER_KEY, CONSUMER_SECRET,
            ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
            var user = User.GetAuthenticatedUser();
            // Search the tweets containing tweetinvi
            var tweets = Search.SearchTweets(palabra);
            foreach (var mention in tweets)
            {

            }

        }

        //hacer favorito
        public bool HacerFavorito(Menciones tmp)
        {
            bool success = Tweet.FavoriteTweet(tmp.TweetID);
            return success;
        }

        //hacer retweet
        public void HacerRetweet(Menciones temp)
        {
            var retweet = Tweet.PublishRetweet(temp.TweetID);
        }

        //contestar tweet
        public void ContestarTweet(Menciones temp, string texto)
        {
            var tweetToReplyTo = Tweet.GetTweet(temp.TweetID);

            // We must add @screenName of the author of the tweet we want to reply to
            var textToPublish = string.Format("@{0} {1}", tweetToReplyTo.CreatedBy.ScreenName, texto);
            var tweet = Tweet.PublishTweetInReplyTo(textToPublish, temp.TweetID);
            Console.WriteLine("Publish success? {0}", tweet != null);
        }


        /*Esta funcion inserta la promocion en twitter y la 
         actualiza el id de promocion que despues guardaremos en la 
         base de datos*/
        public Promocion InsertarPromocion(Promocion tmp)
        {
            var tweet = Tweet.PublishTweetWithImage(tmp.Titulo,tmp.imagen);
            tmp.IdPromocion = tweet.Id;  
            return tmp;
        }

        /*Funcion que se usa para obtener toda la informacion 
         sobre el ganador de la promocion*/
        public Followers obtenerInformacionUsuario(long id)
        {
            var user = User.GetUserFromId(id);
            Followers tmp = new Followers();
            tmp.id_follower = user.Id;
            tmp.Nombre = user.Name;
            tmp.Imagen = user.ProfileImageUrl400x400;
            tmp.Fecha_Creacion = user.CreatedAt;
            tmp.ContSeguidores = user.FollowersCount;
            tmp.ContFav = user.FavouritesCount;
            tmp.estado = user.Description;
            tmp.following = user.Following;

            return tmp;
        }

        /*Obtenemos los usuario que han retwiteado una promocion*/
        public List<Followers> usuarioRetweet(Promocion tmp_promo)
        {
            var tweetId = tmp_promo.IdPromocion;
            var retweeterIds = Tweet.GetRetweetersIds(tweetId);
            
            var retweeters = User.GetUsersFromIds(retweeterIds);
            List<Followers> followsRetweet = new List<Followers>();
            foreach (var twtid in retweeters)
            {
                Followers tmp = new Followers();
                tmp.id_follower = twtid.Id;
                tmp.Nombre = twtid.Name;
                tmp.Imagen = twtid.ProfileImageUrl400x400;
                tmp.Fecha_Creacion = twtid.CreatedAt;
                tmp.ContSeguidores = twtid.FollowersCount;
                tmp.ContFav = twtid.FavouritesCount;
                tmp.estado = twtid.Description;
                tmp.following = twtid.Following;
                followsRetweet.Add(tmp);
            }
            return followsRetweet;
        }


    }
}
