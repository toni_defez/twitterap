﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using capa_modelo;
using capa_negocios;
using System.Collections;

namespace AplicacionTwitter
{
    public partial class F_TimeLine : Form
    {
        Negocio neg;
        DatosUsuario user;

        public F_TimeLine(Negocio neg)
        {
            InitializeComponent();
            this.neg = neg;
            user = neg.DarDatosUsuario();
            MostrarTimeLine();
        }

        public void MostrarTimeLine()
        {

            List<MiTweet> misTweets = neg.getMisTweets();
            tableLayoutPanel1.Hide();
            for (int i = 0; i < misTweets.Count; i++)
            {
                MiTweet c_Tweet = misTweets[i];
                /*Elementos que van a estar en la segunda columna*/
                Label nombre = new Label();
                nombre.AutoSize = true;
                nombre.Font= new Font(FontFamily.GenericMonospace,
                            14.0F, FontStyle.Bold);

                Label text = new Label();
                text.AutoSize = true;
                text.MaximumSize = new Size(400, 0);
                text.Font = new Font(FontFamily.GenericMonospace,
                    12.0f,FontStyle.Italic);

                Label fav = new Label();

                Label retweets = new Label();
                Label fecha_creacion = new Label();
                fecha_creacion.Font = new Font(FontFamily.GenericSerif,
                 8.0f, FontStyle.Italic);

                /*Elementos que van a a estar en la primera columna*/
                PictureBox imagen = new PictureBox();
                imagen.Size = new Size(100, 100);
                imagen.SizeMode = PictureBoxSizeMode.StretchImage;

                /*Efecto de redondeo a la fotografia*/
                System.Drawing.Drawing2D.GraphicsPath gp =
                new System.Drawing.Drawing2D.GraphicsPath();
                gp.AddEllipse(0, 0, imagen.Width - 3, imagen.Height - 3);
                Region rg = new Region(gp);
                imagen.Region = rg;


                FlowLayoutPanel flp = new FlowLayoutPanel();
                flp.AutoSize = true;
                flp.FlowDirection = FlowDirection.TopDown;

                tableLayoutPanel1.AutoScroll = true;
                imagen.ImageLocation = misTweets[i].imagen_usuario;
                nombre.Text = "Usuario: " + misTweets[i].nombre_usuario;
                text.Text = misTweets[i].text;
                fecha_creacion.Text = "Created at: " + misTweets[i].time;
                /*PARA LOS LIKES Y RETWEETS NECESITO ESTO*
                 */
                FlowLayoutPanel likes_retweets = new FlowLayoutPanel();
                likes_retweets.AutoSize = true;
                likes_retweets.FlowDirection = FlowDirection.LeftToRight;

                fav.Text = "likes:" + misTweets[i].favourites;
                retweets.Text = "Retweets:" + misTweets[i].retweets;

                tableLayoutPanel1.Controls.Add(imagen);
                tableLayoutPanel1.SetRow(imagen, i);
                tableLayoutPanel1.SetColumn(imagen, 0);

                flp.Controls.Add(nombre);
                flp.Controls.Add(text);
                Button btn = new Button();
                btn.Text = "Generar Informe";
                btn.Click += delegate
                {
                    InformeTweet t = new InformeTweet(c_Tweet);
                    t.Show();
                };

                likes_retweets.Controls.Add(fav);
                likes_retweets.Controls.Add(retweets);
                likes_retweets.Controls.Add(btn);

                //aqui meto la imagen del tweet
                if (misTweets[i].imagen_tweet != null)
                {
                    PictureBox imagen_tweet = new PictureBox();
                    imagen_tweet.ImageLocation = misTweets[i].imagen_tweet;
                    imagen_tweet.Size = new Size(400, 400);
                    imagen_tweet.SizeMode = PictureBoxSizeMode.StretchImage;
                    flp.Controls.Add(imagen_tweet);
                }

                flp.Controls.Add(likes_retweets);
                flp.Controls.Add(fecha_creacion);
                flp.BackColor = Color.Gray;
                flp.Padding = new System.Windows.Forms.Padding(5);
                flp.Dock = System.Windows.Forms.DockStyle.Fill;



                tableLayoutPanel1.Controls.Add(flp);


                tableLayoutPanel1.SetRow(flp, i);
                tableLayoutPanel1.SetColumn(flp, 1);
                tableLayoutPanel1.RowCount++;
            }
            tableLayoutPanel1.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            neg.IniciarTimeLine();
            tableLayoutPanel1.Hide();
            tableLayoutPanel1.ResetText();
            tableLayoutPanel1.Controls.Clear();
            tableLayoutPanel1.Show();
            MostrarTimeLine();
        }
    }



}
