﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using capa_modelo;
using capa_negocios;
using System.IO;

namespace AplicacionTwitter
{
    public partial class ModificarTweet : Form
    {
        F_Programado padre;
        Negocio neg;
        TweetProgramado tweet;
        string url_imagen;
        byte[] arrayBytes;

        public ModificarTweet(F_Programado padre,Negocio neg,TweetProgramado pep)
        {
            InitializeComponent();
            this.padre = padre;
            this.neg = neg;
            this.tweet = pep;
            arrayBytes = pep.arrayBytes;
            RellenarCampos();
        }

        public static Image Convertir_Bytes_Imagen(byte[] bytes)
        {
            if (bytes == null) return null;

            MemoryStream ms = new MemoryStream(bytes);
            Bitmap bm = null;
            try
            {
                bm = new Bitmap(ms);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            return bm;
        }





        public void RellenarCampos()
        {
            txt_mensaje.Text = tweet.titulo;
            txt_dia.Value = tweet.fechaProgramacion;
            string hora_minuto = tweet.fechaProgramacion.ToShortTimeString();//saco la hora
            string[] datos_horas = hora_minuto.Split(':');
            num_hora.Value = Convert.ToDecimal(datos_horas[0]);
            num_minuto.Value = Convert.ToDecimal(datos_horas[1]);
            url_imagen = tweet.imagen;
            ck_programado.Checked = tweet.programado;
            //aqui tengo que cargar la foto 
            if (!tweet.imagen.Equals(""))
                pictureBox1.Image =
                        Convertir_Bytes_Imagen(tweet.arrayBytes);

        }

        private TweetProgramado CrearTweet()
        {
            int id2 = tweet.Id;
            string usuario = tweet.usuario;
            string titulo = txt_mensaje.Text;
            bool resp = ck_programado.Checked;
            String dia = txt_dia.Value.ToShortDateString();
            String hora = num_hora.Value+":"+num_minuto.Value;
            string fecha = dia + " " + hora;
            DateTime t = Convert.ToDateTime(fecha);
            TweetProgramado tmp;
            if (url_imagen.Equals(""))
            {
                tmp = new TweetProgramado(id2, usuario, titulo, resp, t);
            }
            else
            {
                tmp = new TweetProgramado(id2, usuario, titulo, resp, t,url_imagen);
                tmp.arrayBytes = arrayBytes;
            }

            return tmp;
        }

        private void btn_Acceptar_Click(object sender, EventArgs e)
        {
            TweetProgramado tmp = CrearTweet();

            if (tmp.Equals(tweet))
            {
                //no se han producido cambio 
                txt_error.Text = "No se ha llevado a cabo la modificacion";
            }
            else
            {
                if (neg.ModificarTweetProgramado(tmp) == 0)
                {
                    txt_error.Text = "Se ha llevado a cabo la modificacion";
                }
                else
                {
                    txt_error.Text = "No se ha llevado a cabo la modificacion";
                }
            }

        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            padre.MostrarTweetsProgramados();
            padre.Show();
            this.Close();
        }

        private void btn_cambiarImagen_Click(object sender, EventArgs e)
        {
            try
            {
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    //mostramos la imagen en la interfaz
                    url_imagen = openFileDialog1.FileName;
                    pictureBox1.Image = Image.FromFile(url_imagen);

                    ImageConverter _imageConverter = new ImageConverter();
                    byte[] arrayfoto = (byte[])_imageConverter.ConvertTo(
                        Image.FromFile(url_imagen), typeof(byte[]));
                    arrayBytes = arrayfoto;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("El archivo seleccionado no es un tipo de imagen válido");
            }
        }

        //borrar imagenes
        private void button1_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = null;
            url_imagen = "";//cadena vacia
            arrayBytes = null;
        }
    }
}
