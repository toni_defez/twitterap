﻿namespace AplicacionTwitter
{
    partial class CambiarContra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_clave1 = new System.Windows.Forms.TextBox();
            this.txt_clave2 = new System.Windows.Forms.TextBox();
            this.Aceptar = new System.Windows.Forms.Button();
            this.lbl_user = new System.Windows.Forms.Label();
            this.errorContra = new System.Windows.Forms.ErrorProvider(this.components);
            this.txt_error = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorContra)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(67, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(209, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Introduzca nueva contraseña";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(67, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nueva clave";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(67, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "Repita Clave";
            // 
            // txt_clave1
            // 
            this.txt_clave1.Location = new System.Drawing.Point(194, 94);
            this.txt_clave1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_clave1.Name = "txt_clave1";
            this.txt_clave1.Size = new System.Drawing.Size(116, 23);
            this.txt_clave1.TabIndex = 3;
            // 
            // txt_clave2
            // 
            this.txt_clave2.Location = new System.Drawing.Point(194, 125);
            this.txt_clave2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_clave2.Name = "txt_clave2";
            this.txt_clave2.Size = new System.Drawing.Size(116, 23);
            this.txt_clave2.TabIndex = 4;
            // 
            // Aceptar
            // 
            this.Aceptar.BackColor = System.Drawing.SystemColors.Control;
            this.Aceptar.Location = new System.Drawing.Point(133, 189);
            this.Aceptar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Aceptar.Name = "Aceptar";
            this.Aceptar.Size = new System.Drawing.Size(112, 28);
            this.Aceptar.TabIndex = 5;
            this.Aceptar.Text = "Acceptar";
            this.Aceptar.UseVisualStyleBackColor = false;
            this.Aceptar.Click += new System.EventHandler(this.Aceptar_Click);
            // 
            // lbl_user
            // 
            this.lbl_user.AutoSize = true;
            this.lbl_user.Location = new System.Drawing.Point(67, 60);
            this.lbl_user.Name = "lbl_user";
            this.lbl_user.Size = new System.Drawing.Size(0, 18);
            this.lbl_user.TabIndex = 6;
            // 
            // errorContra
            // 
            this.errorContra.ContainerControl = this;
            // 
            // txt_error
            // 
            this.txt_error.AutoSize = true;
            this.txt_error.Font = new System.Drawing.Font("Rockwell", 10F, System.Drawing.FontStyle.Bold);
            this.txt_error.ForeColor = System.Drawing.Color.Red;
            this.txt_error.Location = new System.Drawing.Point(12, 33);
            this.txt_error.Name = "txt_error";
            this.txt_error.Size = new System.Drawing.Size(419, 18);
            this.txt_error.TabIndex = 7;
            this.txt_error.Text = "Al ser la primera vez que entra debe cambiar la contraseña";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.lbl_user);
            this.groupBox1.Controls.Add(this.Aceptar);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txt_clave2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txt_clave1);
            this.groupBox1.Font = new System.Drawing.Font("Rockwell", 10F, System.Drawing.FontStyle.Bold);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.groupBox1.Location = new System.Drawing.Point(12, 55);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(407, 239);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            // 
            // CambiarContra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 321);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txt_error);
            this.Font = new System.Drawing.Font("Rockwell", 10F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "CambiarContra";
            this.Text = "Cambiar Contraseña";
            ((System.ComponentModel.ISupportInitialize)(this.errorContra)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_clave1;
        private System.Windows.Forms.TextBox txt_clave2;
        private System.Windows.Forms.Button Aceptar;
        private System.Windows.Forms.Label lbl_user;
        private System.Windows.Forms.ErrorProvider errorContra;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label txt_error;
    }
}