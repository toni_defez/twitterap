﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using capa_negocios;
using capa_modelo;

namespace AplicacionTwitter
{
    public partial class F_InsertarTweet : Form
    {
        Negocio neg;
        DatosUsuario dato_user;
        bool foto = false;
        byte[] arrayfoto;
        string url_imagen;

        public F_InsertarTweet(Negocio neg)
        {
            InitializeComponent();
            this.neg = neg;
            dato_user = neg.DarDatosUsuario();
            InicializarElementos();
    
        }

        private void InicializarElementos()
        {
            picture_user.ImageLocation = dato_user.imagen;
            picture_user.SizeMode = PictureBoxSizeMode.StretchImage;
            //obtengo la imagen circular
            System.Drawing.Drawing2D.GraphicsPath gp = new System.Drawing.Drawing2D.GraphicsPath();
            gp.AddEllipse(0, 0, picture_user.Width - 3, picture_user.Height - 3);
            Region rg = new Region(gp);
            picture_user.Region = rg;
        }

        /*solo para enviar un tweet en directo*/
        private void enviarTweetNoProgramado()
        {
            string mensaje = txt_tweet.Text.ToString();
            if (!foto)  //se envia un tweet normal
                neg.EnviarTweet(mensaje);
            else // se envia un tweet con foto
            {
                neg.EnviarTweetFoto(mensaje, url_imagen);
            }

            MessageBox.Show("Se ha enviado correctamente el tweet"
            , "Tweet enviado", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /*solo para los tweet programados*/
        private void enviarTweetProgramado()
        {
            TweetProgramado tp = CrearTweet();
            int result = neg.AgregarTweet(tp);
            if (result == 0) //se ha llevado a cabo la insercion
            {
                MessageBox.Show("Se ha guardado correctamente el tweet"
                    , "Tweet guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("No se  guardado correctamente el tweet"
                   , "Tweet guardado", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }




        /*Guarda en tweet como programado en la base de datos*/
        private void btn_guardar_Click(object sender, EventArgs e)
        {
           
            if (ck_programado.Checked)
            {

                enviarTweetProgramado();
            }
            else
            {
                enviarTweetNoProgramado();
            }

            //reincio la pantalla
            reset();
            

        }


        public void reset()
        {
            txt_tweet.ResetText();
            txt_dia.ResetText();
            num_hora.ResetText();
            num_minuto.ResetText();
            ck_programado.Checked = false;
            pictureBox1.Image = null;
        }


        /*La forma de guardar la fecha es provisionals*/
        private TweetProgramado CrearTweet()
        {
            string mensaje = txt_tweet.Text.ToString();
            if (mensaje.Length > 128)
            {
                mensaje = mensaje.Substring(0, 128);
            }
            string dia = txt_dia.Value.Date.ToShortDateString();
            string hora = num_hora.Value + ":" + num_minuto.Value;
            string fecha = dia + " " + hora;
            DateTime t = Convert.ToDateTime(fecha);
            string usuario = neg.getUserApp().Usuario;
            TweetProgramado tp;
            if (!foto) //se crea un tweet sin foto
            {
                tp = new TweetProgramado(mensaje, usuario, true, t);
            }
            else
            {
                tp = new TweetProgramado(mensaje, usuario, true, t, url_imagen);
                tp.arrayBytes = arrayfoto;
            }
            return tp;
        }

        private void ck_programado_CheckedChanged(object sender, EventArgs e)
        {
            if (ck_programado.Checked == true)
            {
                grp_programado.Visible = true;

            }
            else
            {
                grp_programado.Visible = false;
            }
        }

        private void btn_cargarFoto_Click(object sender, EventArgs e)
        {
            try
            {
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    //mostramos la imagen en la interfaz
                    url_imagen = openFileDialog1.FileName;
                    pictureBox1.Image = Image.FromFile(url_imagen);
                    //guardamos el arraydebytes
                    ImageConverter _imageConverter = new ImageConverter();
                    arrayfoto = (byte[])_imageConverter.ConvertTo(
                        Image.FromFile(url_imagen), typeof(byte[]));
                    foto = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("El archivo seleccionado no es un tipo de imagen válido");
            }
        }

        private void txt_tweet_TextChanged(object sender, EventArgs e)
        {
            int cont_letras = txt_tweet.Text.Length;
            int restantes = 128 - cont_letras;
            if (restantes > 0)
            {
                lbl_contador.Text = restantes + "";
                lbl_contador.BackColor = Color.DarkGray;
            }
            else
            {
                lbl_contador.Text = restantes + "";
                lbl_contador.BackColor = Color.Red;
            }

        }
    }
}
