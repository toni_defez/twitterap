﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using capa_negocios;
using capa_modelo;

namespace AplicacionTwitter
{
    public partial class CambiarContra : Form
    {
        bool validado;
        Login padre;
        Negocio neg;
        
        public CambiarContra(Login padre)
        {
            InitializeComponent();
            this.padre = padre;
            neg = padre.getNegocio();
            lbl_user.Text = neg.getUserApp().Usuario.ToString();
        }

        private void Validar(object sender, CancelEventArgs e)
        {
            TextBox text;
            text = (TextBox)sender;
            if (text.Text.Equals(""))
            {
                text.BackColor = Color.Red;
                e.Cancel = true;
                errorContra.SetError(text, "No puede estar vacio");
                validado = false;
            }
            else
            {
                text.BackColor = Color.White;
                errorContra.Clear();
            }
        }

        private bool Soniguales()
        {
            string clave1 = txt_clave1.Text.ToString();
            string clave2 = txt_clave2.Text.ToString();

            return clave1.Equals(clave2);
        }

        private void Aceptar_Click(object sender, EventArgs e)
        {
            CancelEventArgs te = new CancelEventArgs();
            validado = true;

            Validar(txt_clave1, te);
            Validar(txt_clave2, te);

            if (validado && Soniguales())
            {
                int result=neg.CambiarContra(txt_clave1.Text.ToString());

                if (result == 0)
                {
                    //reincio los labels
                    padre.Reiniciar();
                    padre.setNegocio(neg);
                    padre.Show();
                    this.Close();
                }
                else
                {
                    txt_error.Text = "Error conecte con administrador";
                }
            }
            else
            {
                txt_error.Text = "Las dos claves tienen que ser iguales";
            }
        }
    }
    
}
