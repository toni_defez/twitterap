﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using capa_modelo;  //por ahora no se usa
using capa_negocios;
namespace AplicacionTwitter
{
    public partial class Login : Form
    {
        private Negocio neg;
        private bool validado;
        private int contador;
        private bool admin;
        public Login()
        {
            InitializeComponent();
            neg = new Negocio();
            contador = 3;

        }

        /*Funcion generica para la validacion de los campos
         del formulario*/
        private void Validar(object sender, CancelEventArgs e)
        {
            TextBox text;
            text = (TextBox)sender;
            if (text.Text.Equals(""))
            {
                text.BackColor = Color.Red;
                e.Cancel = true;
                errorLogin.SetError(text, "No puede estar vacio");
                validado = false;
            }
            else
            {
                text.BackColor = Color.White;
                errorLogin.Clear();
            }
        }

        private bool esAdmin()
        {
            String user = txt_usuario.Text.ToString();
            String pass = txt_password.Text.ToString();

            if (user.Equals("admin") && pass.Equals("admin"))
                return true;
            else
                return false;
        }

        /*Reinciar la pantalla*/
        public void Reiniciar()
        {
            contador = 3;
            txt_password.Text = "";
            txt_usuario.Text = "";
            
        }

        private void IniciarApp()
        {
            if (!admin)
                neg.CargarProgramados();
 
            App miApp = new App(neg,this);
            miApp.Show();
            // this.Close();
            this.Hide();
            Reiniciar();
        }

        private void MostrarError() //falta depurar los fallos
        {
            contador--;
            lb_error.BackColor = Color.Red;

            lb_error.Text = "Error de autenticacion Fallos restantes:" + contador;
           
        }

        private void GestionarUsuario()
        {
            string usuario = txt_usuario.Text.ToString();
            string pass = txt_password.Text.ToString();
            //comprobar en el base de datos que el usuario exista
            int resutl = neg.ComprobarUsuario(usuario, pass);
            if (resutl == 2) //exito y es primera vez que entra tiene que cambiar la contraseña
            {
                CambiarContra t2 = new CambiarContra(this);
                this.Hide();
                t2.Show();
            }
            else if (resutl == 0)
            {
                IniciarApp();
            }
            else
            {
                MostrarError();
            }
        }


        private void btn_enviar_Click(object sender, EventArgs e)
        {
            CancelEventArgs te = new CancelEventArgs();
            validado = true;

            Validar(txt_usuario, te);
            Validar(txt_password, te);

            if (contador > 0)
            {
                if (validado && esAdmin()) //campos correctos y solo admin
                {
                    admin = true;
                    IniciarApp();
                }
                else if (validado) //no es admin toca mirar si existe usuario
                {
                    admin = false;
                    GestionarUsuario();
                }
                else
                {
                    MostrarError();
                }
            }
            else
            {
                this.Close();
            }
        }


        public Negocio getNegocio()
        {
            return this.neg;
        }

        public void setNegocio(Negocio neg)
        {
            this.neg = neg;
        }
        /*Reinciar la aplicacion*/
        public void reset()
        {
            neg = null;
            neg = new Negocio();
            contador = 3;
        }
    }
}
