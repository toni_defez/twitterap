﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using capa_modelo;
namespace AplicacionTwitter
{
    public partial class InformeTweet : Form
    {
        public List<MiTweet> l_tweet;
        public InformeTweet(MiTweet elegido)
        {
            l_tweet = new List<MiTweet>();
            l_tweet.Add(elegido);
           
            InitializeComponent();
        }

        private void InformeTweet_Load(object sender, EventArgs e)
        {
            reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.RefreshReport();
            reportViewer1.LocalReport.EnableExternalImages = true;
            reportViewer1.LocalReport.DataSources.Add(
                new ReportDataSource("tablaTweet", l_tweet));
            this.reportViewer1.RefreshReport();
            this.reportViewer1.RefreshReport();
        }
    }
}
