﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using capa_modelo;
using capa_negocios;

namespace AplicacionTwitter
{
    public partial class F_followers : Form
    {
        private Negocio neg;
        private List<Followers> listaFollowers;
        public F_followers(Negocio neg)
        {
            this.neg = neg;
            InitializeComponent();
            listaFollowers = neg.ObtenerFollowers();
            cargarFollowers();
        }

        public void cargarFollowers()
        {
            for (int i = 0; i < listaFollowers.Count; i++)
            {
                Label nombre = new Label();
                nombre.Font = new Font(FontFamily.GenericSansSerif,
                            12.0F, FontStyle.Bold);
                nombre.AutoSize = true;
                Label descripcion = new Label();
                nombre.Font = new Font(FontFamily.GenericMonospace,
                           12.0F, FontStyle.Italic);
                descripcion.AutoSize = true;
                descripcion.MaximumSize = new Size(400, 0);

                Label fav = new Label();
                Label seguidores = new Label();
                Label fecha = new Label();

                PictureBox imagen = new PictureBox();
                imagen.Size = new Size(100, 100);
                imagen.SizeMode = PictureBoxSizeMode.StretchImage;

                //dandole redondez a la imagen
                System.Drawing.Drawing2D.GraphicsPath gp 
                    = new System.Drawing.Drawing2D.GraphicsPath();
                gp.AddEllipse(0, 0, imagen.Width - 3, imagen.Height - 3);
                Region rg = new Region(gp);
                imagen.Region = rg;


                FlowLayoutPanel flp = new FlowLayoutPanel();
                flp.AutoSize = true;
                flp.FlowDirection = FlowDirection.TopDown;

                tableLayoutPanel1.AutoScroll = true;

                imagen.ImageLocation = listaFollowers[i].Imagen;
                nombre.Text = "usuario: " + listaFollowers[i].Nombre;
                seguidores.Text = "follows: " +listaFollowers[i].ContSeguidores;
                fav.Text = "FAVS: " + listaFollowers[i].ContFav;

                descripcion.Text = listaFollowers[i].estado;
                fecha.Text = listaFollowers[i].Fecha_Creacion.ToShortDateString();

                FlowLayoutPanel detalles = new FlowLayoutPanel();
                detalles.AutoSize = true;
                detalles.FlowDirection = FlowDirection.RightToLeft;

                detalles.Controls.Add(seguidores);
                detalles.Controls.Add(fav);




                tableLayoutPanel1.Controls.Add(imagen);
                tableLayoutPanel1.SetRow(imagen, i);
                tableLayoutPanel1.SetColumn(imagen, 0);

                flp.Controls.Add(nombre);
                flp.Controls.Add(descripcion);
                flp.Controls.Add(detalles);
                flp.Controls.Add(fecha);
              

                tableLayoutPanel1.Controls.Add(flp);
                tableLayoutPanel1.SetRow(flp, i);
                tableLayoutPanel1.SetColumn(flp, 1);
                tableLayoutPanel1.RowCount++;
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            InformeFollowers fow = new InformeFollowers(listaFollowers);
            fow.Show();
        }
    }
}
