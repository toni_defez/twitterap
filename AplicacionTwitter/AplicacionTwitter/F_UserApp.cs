﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using capa_negocios;
using capa_modelo;
namespace AplicacionTwitter
{
    public partial class F_UserApp : Form
    {
        private Negocio neg;
        private DatosUsuario midatos;
        public F_UserApp(Negocio neg)
        {
            InitializeComponent();
            this.neg = neg;
            midatos = neg.DarDatosUsuario();
            mostrarDatosUsuario();
        }

        private void mostrarDatosUsuario()
        {
            UserApp miUsuario = neg.getUserApp();
            dataGridView1.Rows.Clear();
            //rellenando los datos de Mi usuario
            dataGridView1.Rows[0].Cells[0].Value = miUsuario.IDUsuario;
            dataGridView1.Rows[0].Cells[1].Value = miUsuario.Usuario;
            dataGridView1.Rows[0].Cells[2].Value = miUsuario.Email;
            dataGridView1.Rows[0].Cells[3].Value = miUsuario.Nombre;
            dataGridView1.Rows[0].Cells[4].Value = miUsuario.Apellidos;

            //mostrar Datos del usuario obtenidos de twitter
            pc_perfil.ImageLocation = midatos.imagen;
            pc_perfil.SizeMode = PictureBoxSizeMode.StretchImage;

            //obtengo la imagen circular
            System.Drawing.Drawing2D.GraphicsPath gp = new System.Drawing.Drawing2D.GraphicsPath();
            gp.AddEllipse(0, 0, pc_perfil.Width - 3, pc_perfil.Height - 3);
            Region rg = new Region(gp);
            pc_perfil.Region = rg;

            lbl_name.Text = miUsuario.Nombre;
            lbl_descripcion.Text = midatos.descripcion;
            lbl_followers.Text = "Seguidores:" + midatos.ContSeguidores;
            lbl_friends.Text = "Amigos:" + midatos.ContAmigos;
            lbl_lengua.Text = "Lengua:" + midatos.lengua;
            lbl_location.Text = "Localizacion:" + midatos.location;

        }


    }
}
