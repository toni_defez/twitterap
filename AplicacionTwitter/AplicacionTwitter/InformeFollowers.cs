﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using capa_modelo;
using Microsoft.Reporting.WinForms;

namespace AplicacionTwitter
{
    public partial class InformeFollowers : Form
    {
        public List<Followers> l_followers;
        public InformeFollowers(List<Followers> misfollows)
        {
            l_followers = misfollows;
            InitializeComponent();
        }

        private void InformeFollowers_Load(object sender, EventArgs e)
        {
            reportViewer1.LocalReport.DataSources.Clear();
            //para poder cargar imagenes desde fuera
            reportViewer1.LocalReport.EnableExternalImages = true;
            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("FollowersInic", l_followers));
            this.reportViewer1.RefreshReport();
        }
    }
}
