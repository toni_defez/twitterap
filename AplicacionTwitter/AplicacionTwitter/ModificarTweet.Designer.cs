﻿namespace AplicacionTwitter
{
    partial class ModificarTweet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Acceptar = new System.Windows.Forms.Button();
            this.txt_error = new System.Windows.Forms.Label();
            this.btn_salir = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.num_minuto = new System.Windows.Forms.NumericUpDown();
            this.num_hora = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btn_cambiarImagen = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ck_programado = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_dia = new System.Windows.Forms.DateTimePicker();
            this.txt_mensaje = new System.Windows.Forms.RichTextBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_minuto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_hora)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_Acceptar
            // 
            this.btn_Acceptar.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn_Acceptar.Font = new System.Drawing.Font("Rockwell", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Acceptar.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_Acceptar.Location = new System.Drawing.Point(12, 342);
            this.btn_Acceptar.Name = "btn_Acceptar";
            this.btn_Acceptar.Size = new System.Drawing.Size(116, 50);
            this.btn_Acceptar.TabIndex = 5;
            this.btn_Acceptar.Text = "Aplicar Cambios";
            this.btn_Acceptar.UseVisualStyleBackColor = false;
            this.btn_Acceptar.Click += new System.EventHandler(this.btn_Acceptar_Click);
            // 
            // txt_error
            // 
            this.txt_error.AutoSize = true;
            this.txt_error.Font = new System.Drawing.Font("Rockwell", 10F, System.Drawing.FontStyle.Bold);
            this.txt_error.Location = new System.Drawing.Point(141, 298);
            this.txt_error.Name = "txt_error";
            this.txt_error.Size = new System.Drawing.Size(0, 18);
            this.txt_error.TabIndex = 9;
            // 
            // btn_salir
            // 
            this.btn_salir.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn_salir.Font = new System.Drawing.Font("Rockwell", 10F, System.Drawing.FontStyle.Bold);
            this.btn_salir.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_salir.Location = new System.Drawing.Point(372, 342);
            this.btn_salir.Name = "btn_salir";
            this.btn_salir.Size = new System.Drawing.Size(106, 50);
            this.btn_salir.TabIndex = 10;
            this.btn_salir.Text = "Volver";
            this.btn_salir.UseVisualStyleBackColor = false;
            this.btn_salir.Click += new System.EventHandler(this.btn_salir_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.num_minuto);
            this.panel1.Controls.Add(this.num_hora);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.btn_cambiarImagen);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.ck_programado);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txt_dia);
            this.panel1.Controls.Add(this.txt_mensaje);
            this.panel1.Font = new System.Drawing.Font("Rockwell", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(466, 268);
            this.panel1.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(113, 178);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(12, 15);
            this.label2.TabIndex = 26;
            this.label2.Text = ":";
            // 
            // num_minuto
            // 
            this.num_minuto.Location = new System.Drawing.Point(131, 172);
            this.num_minuto.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.num_minuto.Name = "num_minuto";
            this.num_minuto.Size = new System.Drawing.Size(39, 23);
            this.num_minuto.TabIndex = 25;
            // 
            // num_hora
            // 
            this.num_hora.Location = new System.Drawing.Point(70, 172);
            this.num_hora.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.num_hora.Name = "num_hora";
            this.num_hora.Size = new System.Drawing.Size(37, 23);
            this.num_hora.TabIndex = 24;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button1.Font = new System.Drawing.Font("Rockwell", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(291, 185);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(148, 50);
            this.button1.TabIndex = 23;
            this.button1.Text = "Borrar fotografia";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(291, 21);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(149, 93);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // btn_cambiarImagen
            // 
            this.btn_cambiarImagen.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn_cambiarImagen.Font = new System.Drawing.Font("Rockwell", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_cambiarImagen.Location = new System.Drawing.Point(291, 131);
            this.btn_cambiarImagen.Name = "btn_cambiarImagen";
            this.btn_cambiarImagen.Size = new System.Drawing.Size(148, 48);
            this.btn_cambiarImagen.TabIndex = 21;
            this.btn_cambiarImagen.Text = "Cambiar Imagen";
            this.btn_cambiarImagen.UseVisualStyleBackColor = false;
            this.btn_cambiarImagen.Click += new System.EventHandler(this.btn_cambiarImagen_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Rockwell", 10F);
            this.label4.Location = new System.Drawing.Point(3, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 17);
            this.label4.TabIndex = 20;
            this.label4.Text = "Mensaje";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Rockwell", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 144);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 17);
            this.label3.TabIndex = 19;
            this.label3.Text = "Dia";
            // 
            // ck_programado
            // 
            this.ck_programado.AutoSize = true;
            this.ck_programado.Location = new System.Drawing.Point(6, 216);
            this.ck_programado.Name = "ck_programado";
            this.ck_programado.Size = new System.Drawing.Size(106, 19);
            this.ck_programado.TabIndex = 18;
            this.ck_programado.Text = "Programado";
            this.ck_programado.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Rockwell", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 178);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 17);
            this.label1.TabIndex = 16;
            this.label1.Text = "Hora";
            // 
            // txt_dia
            // 
            this.txt_dia.Location = new System.Drawing.Point(70, 144);
            this.txt_dia.Name = "txt_dia";
            this.txt_dia.Size = new System.Drawing.Size(200, 23);
            this.txt_dia.TabIndex = 15;
            // 
            // txt_mensaje
            // 
            this.txt_mensaje.Font = new System.Drawing.Font("Rockwell", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_mensaje.Location = new System.Drawing.Point(70, 21);
            this.txt_mensaje.Name = "txt_mensaje";
            this.txt_mensaje.Size = new System.Drawing.Size(200, 96);
            this.txt_mensaje.TabIndex = 14;
            this.txt_mensaje.Text = "";
            // 
            // ModificarTweet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(526, 404);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_salir);
            this.Controls.Add(this.txt_error);
            this.Controls.Add(this.btn_Acceptar);
            this.Font = new System.Drawing.Font("Rockwell", 8.25F);
            this.Name = "ModificarTweet";
            this.Text = "ModificarTweet";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_minuto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_hora)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_Acceptar;
        private System.Windows.Forms.Label txt_error;
        private System.Windows.Forms.Button btn_salir;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btn_cambiarImagen;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox ck_programado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker txt_dia;
        private System.Windows.Forms.RichTextBox txt_mensaje;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown num_minuto;
        private System.Windows.Forms.NumericUpDown num_hora;
    }
}