﻿namespace AplicacionTwitter
{
    partial class F_InsertarTweet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbl_insertar = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.grp_programado = new System.Windows.Forms.GroupBox();
            this.num_minuto = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.num_hora = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_dia = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btn_cargarFoto = new System.Windows.Forms.Button();
            this.lbl_contador = new System.Windows.Forms.Label();
            this.picture_user = new System.Windows.Forms.PictureBox();
            this.txt_tweet = new System.Windows.Forms.RichTextBox();
            this.ck_programado = new System.Windows.Forms.CheckBox();
            this.btn_guardar = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.grp_programado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_minuto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_hora)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture_user)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // panel2
            // 
            this.panel2.AutoSize = true;
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel2.Controls.Add(this.lbl_insertar);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(10);
            this.panel2.Size = new System.Drawing.Size(654, 66);
            this.panel2.TabIndex = 16;
            // 
            // lbl_insertar
            // 
            this.lbl_insertar.AutoSize = true;
            this.lbl_insertar.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_insertar.Font = new System.Drawing.Font("Rockwell", 30F, System.Drawing.FontStyle.Underline);
            this.lbl_insertar.Location = new System.Drawing.Point(10, 10);
            this.lbl_insertar.Name = "lbl_insertar";
            this.lbl_insertar.Size = new System.Drawing.Size(399, 46);
            this.lbl_insertar.TabIndex = 15;
            this.lbl_insertar.Text = "Insertar nuevo tweet";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 66);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(654, 364);
            this.panel1.TabIndex = 17;
            // 
            // panel3
            // 
            this.panel3.AutoSize = true;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.grp_programado);
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Controls.Add(this.btn_cargarFoto);
            this.panel3.Controls.Add(this.lbl_contador);
            this.panel3.Controls.Add(this.picture_user);
            this.panel3.Controls.Add(this.txt_tweet);
            this.panel3.Controls.Add(this.ck_programado);
            this.panel3.Controls.Add(this.btn_guardar);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Font = new System.Drawing.Font("Rockwell", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(10);
            this.panel3.Size = new System.Drawing.Size(654, 364);
            this.panel3.TabIndex = 16;
            // 
            // grp_programado
            // 
            this.grp_programado.BackColor = System.Drawing.SystemColors.ControlLight;
            this.grp_programado.Controls.Add(this.num_minuto);
            this.grp_programado.Controls.Add(this.label3);
            this.grp_programado.Controls.Add(this.num_hora);
            this.grp_programado.Controls.Add(this.label1);
            this.grp_programado.Controls.Add(this.txt_dia);
            this.grp_programado.Controls.Add(this.label2);
            this.grp_programado.Font = new System.Drawing.Font("Rockwell", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grp_programado.Location = new System.Drawing.Point(7, 136);
            this.grp_programado.Name = "grp_programado";
            this.grp_programado.Size = new System.Drawing.Size(417, 150);
            this.grp_programado.TabIndex = 17;
            this.grp_programado.TabStop = false;
            this.grp_programado.Text = "Tweet programado";
            this.grp_programado.Visible = false;
            // 
            // num_minuto
            // 
            this.num_minuto.Location = new System.Drawing.Point(175, 86);
            this.num_minuto.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.num_minuto.Name = "num_minuto";
            this.num_minuto.Size = new System.Drawing.Size(39, 23);
            this.num_minuto.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(157, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 18);
            this.label3.TabIndex = 15;
            this.label3.Text = ":";
            // 
            // num_hora
            // 
            this.num_hora.Location = new System.Drawing.Point(110, 86);
            this.num_hora.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.num_hora.Name = "num_hora";
            this.num_hora.Size = new System.Drawing.Size(40, 23);
            this.num_hora.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(15, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 35);
            this.label1.TabIndex = 12;
            this.label1.Text = "Dia";
            // 
            // txt_dia
            // 
            this.txt_dia.Location = new System.Drawing.Point(110, 33);
            this.txt_dia.Name = "txt_dia";
            this.txt_dia.Size = new System.Drawing.Size(200, 23);
            this.txt_dia.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(15, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 33);
            this.label2.TabIndex = 7;
            this.label2.Text = "Hora";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(504, 136);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(121, 105);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // btn_cargarFoto
            // 
            this.btn_cargarFoto.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn_cargarFoto.Font = new System.Drawing.Font("Rockwell", 10F, System.Drawing.FontStyle.Bold);
            this.btn_cargarFoto.Location = new System.Drawing.Point(504, 252);
            this.btn_cargarFoto.Name = "btn_cargarFoto";
            this.btn_cargarFoto.Size = new System.Drawing.Size(121, 34);
            this.btn_cargarFoto.TabIndex = 15;
            this.btn_cargarFoto.Text = "Cargar Foto";
            this.btn_cargarFoto.UseVisualStyleBackColor = false;
            this.btn_cargarFoto.Click += new System.EventHandler(this.btn_cargarFoto_Click);
            // 
            // lbl_contador
            // 
            this.lbl_contador.AutoSize = true;
            this.lbl_contador.Font = new System.Drawing.Font("Rockwell", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_contador.Location = new System.Drawing.Point(585, 113);
            this.lbl_contador.Name = "lbl_contador";
            this.lbl_contador.Size = new System.Drawing.Size(32, 18);
            this.lbl_contador.TabIndex = 14;
            this.lbl_contador.Text = "288";
            // 
            // picture_user
            // 
            this.picture_user.Location = new System.Drawing.Point(6, 5);
            this.picture_user.Name = "picture_user";
            this.picture_user.Size = new System.Drawing.Size(103, 95);
            this.picture_user.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picture_user.TabIndex = 4;
            this.picture_user.TabStop = false;
            // 
            // txt_tweet
            // 
            this.txt_tweet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_tweet.Font = new System.Drawing.Font("Rockwell", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_tweet.Location = new System.Drawing.Point(158, 5);
            this.txt_tweet.Name = "txt_tweet";
            this.txt_tweet.Size = new System.Drawing.Size(483, 95);
            this.txt_tweet.TabIndex = 0;
            this.txt_tweet.Text = "";
            this.txt_tweet.TextChanged += new System.EventHandler(this.txt_tweet_TextChanged);
            // 
            // ck_programado
            // 
            this.ck_programado.AutoSize = true;
            this.ck_programado.Font = new System.Drawing.Font("Rockwell", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ck_programado.Location = new System.Drawing.Point(134, 116);
            this.ck_programado.Name = "ck_programado";
            this.ck_programado.Size = new System.Drawing.Size(112, 22);
            this.ck_programado.TabIndex = 9;
            this.ck_programado.Text = "Programado";
            this.ck_programado.UseVisualStyleBackColor = true;
            this.ck_programado.CheckedChanged += new System.EventHandler(this.ck_programado_CheckedChanged);
            // 
            // btn_guardar
            // 
            this.btn_guardar.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn_guardar.Font = new System.Drawing.Font("Rockwell", 10F, System.Drawing.FontStyle.Bold);
            this.btn_guardar.Location = new System.Drawing.Point(269, 310);
            this.btn_guardar.Name = "btn_guardar";
            this.btn_guardar.Size = new System.Drawing.Size(155, 43);
            this.btn_guardar.TabIndex = 10;
            this.btn_guardar.Text = "Generar Tweet";
            this.btn_guardar.UseVisualStyleBackColor = false;
            this.btn_guardar.Click += new System.EventHandler(this.btn_guardar_Click);
            // 
            // F_InsertarTweet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(654, 430);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "F_InsertarTweet";
            this.Text = "F_mdi1";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.grp_programado.ResumeLayout(false);
            this.grp_programado.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_minuto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_hora)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture_user)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lbl_insertar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox grp_programado;
        private System.Windows.Forms.NumericUpDown num_minuto;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown num_hora;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker txt_dia;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btn_cargarFoto;
        private System.Windows.Forms.Label lbl_contador;
        private System.Windows.Forms.PictureBox picture_user;
        private System.Windows.Forms.RichTextBox txt_tweet;
        private System.Windows.Forms.CheckBox ck_programado;
        private System.Windows.Forms.Button btn_guardar;
    }
}