﻿
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using capa_negocios;
using capa_modelo;
using System.Collections;
using System;
using System.IO;

namespace AplicacionTwitter
{
    public partial class App : Form
    {
        private Negocio neg;
        private F_InsertarTweet f_mdi2;
        private F_UserApp f_mdi1;
        private F_TimeLine f_mdi3;
        private F_Programado f_mdi4;
        private F_followers f_mdi5;
        private Login padre;

        public App(Negocio neg,Login padre)
        {
            InitializeComponent();
            this.neg = neg;
            this.padre = padre;

            ComprobarAdmin();


        }

        //esta funcion sirve para cambiar el menu
        // e inicializar el temporizador
        public void ComprobarAdmin()
        {
            if (neg.getUserApp() != null)
            {
                archivoToolStripMenuItem.Visible = false;
                revisarTweet.Start();
            }
            else
            {
                usuarioAppToolStripMenuItem.Visible = false;
                tweetToolStripMenuItem.Visible = false;
                usuarioToolStripMenuItem.Visible = false;
                timeLineToolStripMenuItem.Visible = false;

            }
        }


        private void importarArchivoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CargaFichero();

        }

        private void CargaFichero()
        {
 
            List<UserApp> temp = CrearUserApp();
            int item=0;
            foreach (UserApp t in temp)
            {
                 item = neg.AgregarUsuario(t);
            }
            if (item == 1)  // 1 correcto introduccion
            {
                MessageBox.Show("Error Message", "Se ha introducido", 
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else // 0 incorrecta
            {
                MessageBox.Show("Error Message", "No se ha introducido",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
        }

        private List<UserApp> CrearUserApp()
        {
            //leemos de fichero
            string line;  
            StreamReader file =
               File.OpenText("fichero.txt");
            ArrayList myAL = new ArrayList();

            while ((line = file.ReadLine()) != null)
            {
                myAL.Add(line);
            }
           
            file.Close();

            List<UserApp> user = new List<UserApp>();
            int contador = 0;
            while (contador+9<=myAL.Count)
            {
                string usuario = (string)myAL[contador+0];
                string password = (string)myAL[contador+1];
                string email = (string)myAL[contador+2];
                string nombre = (string)myAL[contador+3];
                string apellidos = (string)myAL[contador+4];
                string consumerKey1 = (string)myAL[contador+5];
                string consumerKey2 = (string)myAL[contador+6];
                string token1 = (string)myAL[contador+7];
                string token2 = (string)myAL[contador+8];
                UserApp temp = new UserApp
                    (usuario, password, email, nombre, apellidos,
                    consumerKey1, consumerKey2, token1, token2);
                user.Add(temp);
                contador+=9;
            }

            return user;
        }



        private void usuarioAppToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cierra_formularios();
            this.IsMdiContainer = false;
            this.IsMdiContainer = true;
            f_mdi1 = new F_UserApp(neg);
            f_mdi1.MdiParent = this;
            f_mdi1.Dock = DockStyle.Fill;
            f_mdi1.Show();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            /*
            cierra_formularios();
            this.IsMdiContainer = false;
            this.IsMdiContainer = true;
            f_mdi1 = new F_UserApp(neg);
            f_mdi1.MdiParent = this;
            f_mdi1.Show();
            */
        }

        private void cierra_formularios()
        {
            // Cierro los formularios abiertos
            for (int i = 1; i < panel1.Controls.Count; i++)
            {
                Form aux = (Form)panel1.Controls[i];
                aux.Close();
            }

            // Borro todos los elementos del panel
            this.Controls.Remove(panel1);
        }

        /*Para cerrar sesion lo que hacemos es cerrar el hijo y abrir el padre*/
        private void cerrarSesionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            padre.Show();
            this.Close();
        }

        private void insertarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cierra_formularios();
            this.IsMdiContainer = false;
            this.IsMdiContainer = true;
            f_mdi2 = new F_InsertarTweet(neg);
            f_mdi2.MdiParent = this;
            f_mdi2.Dock = DockStyle.Fill;
            f_mdi2.Show();
        }

        private void timeLineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cierra_formularios();
            this.IsMdiContainer = false;
            this.IsMdiContainer = true;
            f_mdi3 = new F_TimeLine(neg);
            f_mdi3.MdiParent = this;
            f_mdi3.Dock = DockStyle.Fill;
            f_mdi3.Show();
        }

        private void eliminarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cierra_formularios();
            this.IsMdiContainer = false;
            this.IsMdiContainer = true;
            f_mdi4 = new F_Programado(neg);
            f_mdi4.MdiParent = this;
            f_mdi4.Dock = DockStyle.Fill;
            f_mdi4.Show();
        }


        //este metodo se llamara cada X tiempo para revisar los 
        //tweets que falten por publicar
        private void revisarTweet_Tick(object sender, EventArgs e)
        {
            neg.RevisarTweets();

            //tweets que faltan por publicar
            notifyIcon1.Text = "Mensajes pendientes por enviar  "
                +neg.ObtenerMensajePorEnviar() + "";
        }

        private void followersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cierra_formularios();
            this.IsMdiContainer = false;
            this.IsMdiContainer = true;
            f_mdi5 = new F_followers(neg);
            f_mdi5.MdiParent = this;
            f_mdi5.Dock = DockStyle.Fill;
            f_mdi5.Show();
        }

        private void acercaDeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 t1 = new AboutBox1(this);
            t1.Show();
        }


        private void salirAplicacionToolStripMenuItem_Click(object sender, EventArgs e)
        {
          
            this.Close();
            padre.Close(); 
        }

        private void CerraSesionToolStripMenuItem_Click(object sender, EventArgs e)
        {

            padre.Reiniciar();
            padre.Show();
            this.Close();
        }

  
    }
}
