﻿namespace AplicacionTwitter
{
    partial class F_UserApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel_user = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_descripcion = new System.Windows.Forms.Label();
            this.lbl_followers = new System.Windows.Forms.Label();
            this.lbl_location = new System.Windows.Forms.Label();
            this.lbl_friends = new System.Windows.Forms.Label();
            this.lbl_lengua = new System.Windows.Forms.Label();
            this.lbl_name = new System.Windows.Forms.Label();
            this.pc_perfil = new System.Windows.Forms.PictureBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ID_Usuario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Usuario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Apellidos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel_user.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pc_perfil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.AutoSize = true;
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(10);
            this.panel2.Size = new System.Drawing.Size(638, 68);
            this.panel2.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label1.Font = new System.Drawing.Font("Rockwell", 30F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(460, 46);
            this.label1.TabIndex = 2;
            this.label1.Text = "Informacion del usuario";
            // 
            // panel3
            // 
            this.panel3.AutoSize = true;
            this.panel3.Controls.Add(this.panel_user);
            this.panel3.Controls.Add(this.dataGridView1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 68);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(10);
            this.panel3.Size = new System.Drawing.Size(638, 389);
            this.panel3.TabIndex = 4;
            // 
            // panel_user
            // 
            this.panel_user.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_user.Controls.Add(this.panel1);
            this.panel_user.Controls.Add(this.lbl_name);
            this.panel_user.Controls.Add(this.pc_perfil);
            this.panel_user.Location = new System.Drawing.Point(34, 17);
            this.panel_user.Name = "panel_user";
            this.panel_user.Size = new System.Drawing.Size(559, 220);
            this.panel_user.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lbl_descripcion);
            this.panel1.Controls.Add(this.lbl_followers);
            this.panel1.Controls.Add(this.lbl_location);
            this.panel1.Controls.Add(this.lbl_friends);
            this.panel1.Controls.Add(this.lbl_lengua);
            this.panel1.Location = new System.Drawing.Point(166, 15);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(370, 181);
            this.panel1.TabIndex = 7;
            // 
            // lbl_descripcion
            // 
            this.lbl_descripcion.Font = new System.Drawing.Font("Rockwell", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_descripcion.Location = new System.Drawing.Point(3, 21);
            this.lbl_descripcion.Name = "lbl_descripcion";
            this.lbl_descripcion.Size = new System.Drawing.Size(336, 38);
            this.lbl_descripcion.TabIndex = 1;
            this.lbl_descripcion.Text = "label2";
            // 
            // lbl_followers
            // 
            this.lbl_followers.Font = new System.Drawing.Font("Rockwell", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_followers.Location = new System.Drawing.Point(3, 59);
            this.lbl_followers.Name = "lbl_followers";
            this.lbl_followers.Size = new System.Drawing.Size(253, 23);
            this.lbl_followers.TabIndex = 2;
            this.lbl_followers.Text = "label3";
            // 
            // lbl_location
            // 
            this.lbl_location.Font = new System.Drawing.Font("Rockwell", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_location.Location = new System.Drawing.Point(3, 140);
            this.lbl_location.Name = "lbl_location";
            this.lbl_location.Size = new System.Drawing.Size(253, 23);
            this.lbl_location.TabIndex = 5;
            this.lbl_location.Text = "label6";
            // 
            // lbl_friends
            // 
            this.lbl_friends.Font = new System.Drawing.Font("Rockwell", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_friends.Location = new System.Drawing.Point(3, 94);
            this.lbl_friends.Name = "lbl_friends";
            this.lbl_friends.Size = new System.Drawing.Size(253, 23);
            this.lbl_friends.TabIndex = 3;
            this.lbl_friends.Text = "label4";
            // 
            // lbl_lengua
            // 
            this.lbl_lengua.Font = new System.Drawing.Font("Rockwell", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_lengua.Location = new System.Drawing.Point(3, 117);
            this.lbl_lengua.Name = "lbl_lengua";
            this.lbl_lengua.Size = new System.Drawing.Size(253, 23);
            this.lbl_lengua.TabIndex = 4;
            this.lbl_lengua.Text = "label5";
            // 
            // lbl_name
            // 
            this.lbl_name.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_name.Font = new System.Drawing.Font("Rockwell", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_name.Location = new System.Drawing.Point(17, 167);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(143, 38);
            this.lbl_name.TabIndex = 6;
            this.lbl_name.Text = "label2";
            // 
            // pc_perfil
            // 
            this.pc_perfil.Location = new System.Drawing.Point(20, 15);
            this.pc_perfil.Name = "pc_perfil";
            this.pc_perfil.Size = new System.Drawing.Size(140, 140);
            this.pc_perfil.TabIndex = 0;
            this.pc_perfil.TabStop = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Rockwell", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.NullValue = null;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(5);
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridView1.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID_Usuario,
            this.Usuario,
            this.Email,
            this.Nombre,
            this.Apellidos});
            this.dataGridView1.Enabled = false;
            this.dataGridView1.Location = new System.Drawing.Point(34, 243);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(559, 45);
            this.dataGridView1.TabIndex = 4;
            // 
            // ID_Usuario
            // 
            this.ID_Usuario.Frozen = true;
            this.ID_Usuario.HeaderText = "ID_Usuario";
            this.ID_Usuario.Name = "ID_Usuario";
            this.ID_Usuario.ReadOnly = true;
            this.ID_Usuario.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Usuario
            // 
            this.Usuario.Frozen = true;
            this.Usuario.HeaderText = "Usuario";
            this.Usuario.Name = "Usuario";
            this.Usuario.ReadOnly = true;
            this.Usuario.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Usuario.Width = 110;
            // 
            // Email
            // 
            this.Email.Frozen = true;
            this.Email.HeaderText = "Email";
            this.Email.Name = "Email";
            this.Email.ReadOnly = true;
            this.Email.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Email.Width = 250;
            // 
            // Nombre
            // 
            this.Nombre.Frozen = true;
            this.Nombre.HeaderText = "Nombre";
            this.Nombre.Name = "Nombre";
            this.Nombre.ReadOnly = true;
            this.Nombre.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Nombre.Width = 120;
            // 
            // Apellidos
            // 
            this.Apellidos.Frozen = true;
            this.Apellidos.HeaderText = "Apellidos";
            this.Apellidos.Name = "Apellidos";
            this.Apellidos.ReadOnly = true;
            this.Apellidos.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Apellidos.Width = 110;
            // 
            // F_UserApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(638, 457);
            this.ControlBox = false;
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "F_UserApp";
            this.Text = "F_UserApp";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel_user.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pc_perfil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel_user;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbl_descripcion;
        private System.Windows.Forms.Label lbl_followers;
        private System.Windows.Forms.Label lbl_location;
        private System.Windows.Forms.Label lbl_friends;
        private System.Windows.Forms.Label lbl_lengua;
        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.PictureBox pc_perfil;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_Usuario;
        private System.Windows.Forms.DataGridViewTextBoxColumn Usuario;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn Apellidos;
    }
}