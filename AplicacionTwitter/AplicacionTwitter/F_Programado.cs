﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using capa_modelo;
using capa_negocios;

namespace AplicacionTwitter
{
    public partial class F_Programado : Form
    {
        private Negocio neg;
        private DataTable dt;
        private DataView mi_vista;

        public F_Programado(Negocio neg)
        {
            InitializeComponent();
            this.neg = neg;
            IniciarTabla();
            MostrarTweetsProgramados();
            mostrarProgramados.Start();
        }

        public void IniciarTabla()
        {
            dt = new DataTable();
            dt.Columns.Add(new DataColumn("Id", typeof(string)));
            dt.Columns.Add(new DataColumn("Usuario", typeof(string)));
            dt.Columns.Add(new DataColumn("Titulo", typeof(string)));
        
            dt.Columns.Add(new DataColumn("Programado",typeof(bool)));
            dt.Columns.Add(new DataColumn("Fecha_Programacion", typeof(DateTime)));
            dt.Columns.Add(new DataColumn("Imagen", typeof(string)));

            mi_vista = new DataView(dt);
            dataGridView1.DataSource = mi_vista;
            dataGridView1.Columns[0].Width = 100;
            dataGridView1.Columns[1].Width = 100;
            dataGridView1.Columns[2].Width = 200;
            dataGridView1.Columns[3].Width = 100;
            dataGridView1.Columns[4].Width = 140;
            dataGridView1.Columns[5].Width = 120;
        }



        public void  MostrarTweetsProgramados()
        {
            List<TweetProgramado> misTweets = neg.getMisTweetsProgramados();

            int i = 0;

            dt.Clear();
            i = 0;
            while (i < misTweets.Count)
            {
                DataRow row = dt.NewRow();
                row["Id"] = misTweets[i].Id;
                row["Usuario"] = misTweets[i].usuario;
                row["Titulo"] = misTweets[i].titulo;
                row["Programado"] = misTweets[i].programado;
                row["Fecha_Programacion"] = misTweets[i].fechaProgramacion;
                row["Imagen"] = misTweets[i].imagen;
                dt.Rows.Add(row);
                row.AcceptChanges();
                i++;
            }
        }

        private void btn_borrar_Click(object sender, EventArgs e)
        {
            int res, r;

            // Obtengo el indice de la fila seleccionada
            r = dataGridView1.SelectedCells[0].RowIndex;

            res = neg.EliminarTweetProgramado(dataGridView1.Rows[r].Cells[0].Value.ToString());
            if (res == 0)
            {
                MessageBox.Show("Se elimino el Tweet correctamente"
                 , "Borrado de Tweet", MessageBoxButtons.OK, MessageBoxIcon.Error);
                // Elimino el elemnto del DataGrid
                dataGridView1.Rows.RemoveAt(r);
            }
            else
            {
                MessageBox.Show("No se elimino el Tweet correctamente"
                 , "Borrado de Tweet", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private TweetProgramado CrearTweet(int fila)
        {
            int id2 =Convert.ToInt32(dataGridView1.Rows[fila].Cells[0].
                Value.ToString());

            TweetProgramado mod = neg.getTweetProgramdoPorId(id2);
     

            return mod;
        }

        private void btn_editar_Click(object sender, EventArgs e)
        {
            int fila = dataGridView1.SelectedCells[0].RowIndex;
            TweetProgramado TweetElegido = CrearTweet(fila);
            ModificarTweet tp = new ModificarTweet(this,neg,TweetElegido);
            this.Hide();
            tp.Show();
        }



        /*Filtrar por titulo*/
        private void btn_titulo_Click(object sender, EventArgs e)
        {
            String titulo = txt_titulo.Text;
            if (titulo == "")
            {
                titulo = "*";
            }
            mi_vista.RowFilter = "Titulo LIKE '" + titulo + "*'";

        }

        /*Muestra aquellos programados con fecha posterior a la actual*/
        private void btn_pendientes_Click(object sender, EventArgs e)
        {
            //mi_vista.RowFilter = "Fecha_Programacion  > 1/1/1900";
            String fechaActual = DateTime.Now.ToShortDateString();
            mi_vista.RowFilter = "Fecha_Programacion > #" +
                DateTime.Now.ToString("MM/dd/yyyy HH:mm") + "#";
        }

        //Tengo que estar actualizando la tabla para que no me aparezcan lo que 
        //no existen
        private void mostrarProgramados_Tick(object sender, EventArgs e)
        {
            MostrarTweetsProgramados();
            this.Refresh();
        }

    }
}
